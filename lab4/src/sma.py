import openpyxl
import matplotlib.pyplot as plt
import urllib.request

class Danie:
	def get_file(self):
	    try:
	        urllib.request.urlopen("http://google.com")
	    except IOError:
	        return 404
	    url = 'https://cbr.ru/Queries/UniDbQuery/DownloadExcel/98956?Posted=True&mode=1&VAL_NM_RQ=R01235&From=18.05.2020&To=18.06.2020&FromDate=05%2F18%2F2020&ToDate=06%2F18%2F2020'
	    urllib.request.urlretrieve(url, 'danie.xlsx')
	    return 0
	def get_bazovie():
		bazovie = []
		wb = openpyxl.load_workbook('danie.xlsx')
		sheets = wb.sheetnames
		sheet = wb.active
		i = 0
		for cell in sheet['C']:
			bazovie.append(cell.value)
		bazovie.pop(0)
		return bazovie	

	def get_datas(self):
		datas = []	
		wb = openpyxl.load_workbook('danie.xlsx')
		sheets = wb.sheetnames
		sheet = wb.active
		i = 0
		for cell in sheet['B']:
			datas.append(cell.value)
		return datas	

class Raschet:
	def get_preobraz(bazovie):
		preobraz = []
		length = len(bazovie)
		i = 0
		while i < length:
			while i < 5:
				preobraz.append(bazovie[i])
				i = i + 1
			preobraz.append((bazovie[i]+bazovie[i-1]+bazovie[i-2]+bazovie[i-3]+bazovie[i-4])/5)
			i = i + 1
		return preobraz	

class Graphics:
	def get_postroika(preobraz,bazovie):
		length = len(bazovie)
		length = length	
		listvremeni = list(range(0,length))
		i = 0
		fig,ax=plt.subplots()
		plt.xlabel("t")
		plt.ylabel("znach")
		plt.title('graphic SMA')
		plt.plot(listvremeni, preobraz, color = 'red', linestyle = 'solid', label = 'SMA')
		plt.plot(listvremeni, bazovie, color = 'blue', linestyle = 'solid', label = 'Standart Values')
		plt.legend(loc = 'upper right')
		plt.savefig("graph.png")

D = Danie()
R = Raschet()

D.get_file()
guchi = Danie.get_bazovie()
print(guchi)
Graphics.get_postroika(Raschet.get_preobraz(Danie.get_bazovie()),Danie.get_bazovie())