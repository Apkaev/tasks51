import re
from collections import Counter
import matplotlib.pyplot as plt

class Parser:

	full = []

	def get_full(self):
		file = open('src/mbox.txt', 'r')
		full = file.read()
		file.close()
		if not full:
		    return 404
		self.full = full
		return full

	def get_srznach(full):
		Probality = []
		Probality = re.findall(r'X-DSPAM-Probability: (\d{1}.\d{4})', full)
		Probality = [float(x) for x in Probality]
		length = len(Probality)
		summa = sum(Probality)
		srzanch = summa/length	
		return srzanch

	def get_length(self):
		Probality = []
		Probality = re.findall(r'X-DSPAM-Probability: (\d{1}.\d{4})', self.full)
		Probality = [float(x) for x in Probality]
		length = len(Probality)	
		return length

	def get_mail(self):
		mail = re.findall(r'From: (\w+.\w+@\w+)', self.full)
		self.mail = mail
		return mail
				
	def get_z(self):
		z = []
		z = re.findall(r'Date: (\w+, \d+ \w+ \d{4} \d+)', self.full)
		return z		

class Vivod:
	def get_graphic(mail):
		xbar = []
		c = {}
		c = Counter(mail)
		x = c.keys()
		for key in x:
			xbar.append(c[key])	
		fig, ax = plt.subplots()
		ax.bar(x, xbar)
		plt.savefig("screen.png")
		plt.show()
	def get_spamer(length,z,mail):
		spammerlist = []
		strz = []
		strm = []
		dict_povtor = {}
		list_dict = []
		lenmail = []
		i = 0
		while i < length:
			lenmail.append(mail.index(mail[i]))
			i = i + 1

		y = 0
		i = 0
		for i in z:
			if i == strz:
				if mail[y] == strm:
					n = n+1
					dict_povtor[mail[y]] = n
				else:
					n = 1
					dict_povtor[mail[y]] = n
			else:
				if mail[y] == strm:
					n = n+1
					dict_povtor[mail[y]] = n	
				else:
					n = 1
					dict_povtor[mail[y]] = n		
			if z == 0:	
				if dict_povtor[mail[y]] > 5:
					z = 1
					spammerlist.append(mail[y])
					#print('Этого пользователя нужно заблокировать за спам', mail[y])
					#print('Так как он за час отправил',dict_povtor[mail[y]])
					#print('В такую дату', i)
			if mail[y] != strm:
				z = 0										
			strz = i
			strm = mail[y]
			y = y + 1
		return spammerlist	

P = Parser()
#V = Vivod()

dspam = Parser.get_srznach(Parser.get_full(P))
print(dspam)
Vivod.get_graphic(P.get_mail())
spammerlistv = Vivod.get_spamer(P.get_length(),P.get_z(),P.get_mail())
print(spammerlistv)